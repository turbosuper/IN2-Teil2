/*
 * =====================================================================================
 *
 *       Filename:  balancing.c
 *
 *    Description:  Datei mit Funktionen fuer balanzierung der Baum
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ships.h"
#include "balancing.h"
#include "lib.h"
#include "datastructure.h"



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  createList
 *  Description:  Funktion die baut einen Liste aus der Baum
 *  Arguments:    Zeiger auf Anfang der Baum
 *  Return Value: Zeiger auf der ersten Elemnt den neuen Verkette Liste
 * =====================================================================================
 */

LLElement* createList(Ship* root){
	LLElement* list1;
	LLElement* list2;
	LLElement* list3;
	LLElement* last; 
	
	if (root->Left){			//wenn linke zweig existiert
		list1 = createList(root->Left); //aus der linke Zweig Elmente addieren
		}
	else list1 = NULL;			//wenn keine linke Elemente in der Linken Zwieg, dann ist list1 NULL

	if (root->Right){
		list2 = createList(root->Right);
		}
	else list2 = NULL;

	list3 = createElement(root);
	  
     	// Die "Mittlere" list3 darf nicht NULL sein, deswege verbinden wir list2 mit list3
	list3->Next = list2; // Wenn list2 ist NULL, dann ist es OK
       	
	if (!list1) return list3; // Nichts zu addieren
	
	last = list1;
	while (last->Next) last=last->Next; // Zu der Ende der list1 gehen
	
	last->Next = list3; // Append list3+list2 zu der Ende der list1
	
	return list1;
}
		/* -----  end of function createList  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  convertTreeToList
 *  Description:  Funktion der Enkappsselt andere Funktione um die Liste zu erzeugen.
 *  		  Das sind:
 *  		  1. In der Verkette Liste zu einspeichern
 *  		  2. Aus der Liste Markierte Elemente Entfernen
 *  		 
 *  Arguments:    Zieger auf Anfang der Verkette Liste, Wurzel der Baum
 *  Retrun value: Zeiger auf Anfang der Verkette Liste 
 * =====================================================================================
 */
LLElement* convertTreeToList(LLElement* LLFirstElement, Ship* root ){
	LLFirstElement = createList(root);   
	LLFirstElement = deleteMarkedFromLL(LLFirstElement);
//	printList(LLFirstElement);
	return LLFirstElement;
}		/* -----  end of function convertTreeToList  ----- */
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  appendShip
 *  Description:  Einen neuen Shiff addieren zu der letzter Platz in der Verkette Liste
 *  Arguments:    Zeiger auf Anfang der Verkette Liste, Zeiger auf der Shiff
 *  Return Value  Zeiger auf der erster Element - Anfang der verkette Liste
 * =====================================================================================
 */
LLElement* appendShip(LLElement* LLFirstElement,Ship* ship){

	LLElement* new;
        LLElement* current;
        LLElement* prev;
	int i = 1;

	new = createElement(ship);

        if (LLFirstElement == NULL){    	//Wenn gibst keine Elmente bei der Verkette Liste
		 LLFirstElement = new;
		 LLFirstElement->Counter = i; 		//Das wird dann der Ersten Element
                 }
        else {						//wenn es schon ein paar Elmente in der Liste gibt
		current = LLFirstElement;   		//speichern wir der erster Element
	        while(current != NULL){			//suchen wir nach letzte Element - so lange etwas gibt...
		        prev = current;			//...spiechern wir der aktuelen Elment
		        current = current->Next;	// ... und gehen in der nachste Element, der mit dieser Verbunden ist
			i++; 				//erhoehen wir auch damit der Zahler
	                }
		prev->Next = new;
		prev->Next->Counter = i;
		}

         return LLFirstElement;
}
		

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  printList
 *  Description:  Drueck den Ganzen Liste Aus
 *  Argument: 	  Zeiger Auf der erster Elment aus der Liste
 *  Return value: Keine
 * =====================================================================================
 */
void printList(LLElement* LLFirstElement){
	 LLElement* current = LLFirstElement;
	 
	  if (current == NULL){
	         printf("Liste ist leer\n");
	         }
	  else printf("Die Verkette Liste:\n");
	 
	  while(current != NULL){
	          printShip(current->Ship);
		  printf("Zaehler: %d\n", current->Counter);
	          current = current->Next;
	  }
}		/* -----  end of function printList  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  createElement
 *  Description:  Erzeugt einer Element der Verkette Liste
 *  Argumente:	  Zieger Auf Schiff, da wird dieser Element zeigen
 *  Return Werte  Zeiger auf der Element 	
 *  * =====================================================================================
 */
LLElement* createElement(Ship* ship){
	
	LLElement* new;
	
	new = (LLElement*)malloc(sizeof(LLElement));
	new->Ship = ship;
	new->Counter = 0;
	new->Next = NULL;

	return new;
}		/* -----  end of function createElement  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  deleteMarkedFromLL
 *  Description:  Funktion, der loescht markierte Elemente aus der Liste
 *  Arguments:    Zeiger Auf der Anfang der Verkette Liste
 *  Return Value  Zeiger auf der Anfang der Verkette Liste
 * =====================================================================================
 */
LLElement* deleteMarkedFromLL(LLElement* LLFirstElement){
	LLElement* current = LLFirstElement;
	LLElement* temp;
	LLElement* p = NULL;

	 for (current = LLFirstElement; current != NULL; p = current, current = current->Next){
		if (current->Ship->deleteFlag ==1 ){
			if (p == NULL){ 
				LLFirstElement = current->Next;
			}else{
				p->Next = current->Next;
			}
			free(current);
			}
		 }
	return LLFirstElement; 
 }
	

/* -----  end of function deleteMarkedFromLL  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  countElments
 *  Description:  Funktion der setzt Zahler in der Verkette Liste
 *  Arguments: 	  Anfang der Verkette Liste
 *  Return value: Anzahl von Elementen
 * =====================================================================================
 */
int countElements(LLElement* LLFirstElement){
	int i=0;
	LLElement* current;
	current = LLFirstElement;
	while( current != NULL){
		i++;
		current->Counter = i;
		current = current->Next;
		}
	return i;
}		/* -----  end of function countElments  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  balance
 *  Description:  Funktion der balanziert der Baum. Funktioniert nach dieser Methode:
 *  		  -erst eine Verkette Liste aus Der Baum bauen (Fkt convertTreeToList())
 *  		  -Anzahl der Elemente zahlen
 *  		  -Funktion fromListToTree aufrufem
 *  Arguments:    Zeiger auf der Wurzel der alten Baum
 *  Return Value: Zeiger auf der Wurzel den neuen Baum
 *
 * =====================================================================================
 */
Ship* balance(Ship* oldroot){
	Ship* newroot;
	unsigned int counter;
	LLElement* newlist;
	newlist = convertTreeToList(newlist, oldroot);
	printf("Refolg mit erzeugen der neuen List\n");
	counter = countElements(newlist);
	printf("Anzahle der Elemente ist: %d\n", counter);
	newroot = sortedListToBST(newlist, counter);
	return newroot;
}		/* -----  end of function balance  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  sortedListToBST
 *  Description:  Funktion erzeug Baum aufgrund existierende Verkette Liste
 *  Arguments:    Zeiger auf erster Element der Verkette Liste, Anzahl der Elemente der 
 *  		  Liste
 *  Return Value: Zeiger auf der Wurzel der neuen Baum
 * =====================================================================================
 */
Ship* sortedListToBST(LLElement* LLFirstElement, int counter){
	
	if (counter <= 0) return NULL; //nichts machen wenn list leer ist

	LLElement* current;
	Ship* root;
	Ship* left;
	current = LLFirstElement;
	
	/* Erst den Linken Zweig generieren, mit den MITTLEREN Element aus der Liste */
	left = sortedListToBST(current, counter/2); 
	
	/* Spiecher sichern fuer der Wurzel */
	root = newShip(current->Ship);
	root->Left = left; 	//o.g. linken Zwei verbinden
	
	current = current->Next;  //fuer nachtsen rekusiven Aufruf nachster Element aus VL nehmen

	/* Jetzt fuer rechten Zweig sind nur diese Elemente ubrig geblieben
	 * die nicht in den linken benutzen geworden waren. das heist (n-n/2) -1 */
	root->Right = sortedListToBST(current, counter-counter/2-1);

	return root;
}		/* -----  end of function sortedListToBST  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  newShip
 *  Description:  Funktion der baut einen Neuen Shiff als knote der Baum, aus einer 
 *  		  Element der Verkette Liste
 *  Arguments:    Zeiger auf Shif aus der Verkette Liste
 *  Return value: Zeiger auf Shif als Element der BAum
 * =====================================================================================
 */
Ship* newShip(Ship* shipFromList){
	Ship* ship = (Ship*)malloc(sizeof(Ship));
	strcpy(ship->Name, shipFromList->Name);   //strcpy noteig weil das sind zeiger
	ship->Position = shipFromList->Position;
	ship->Speed = shipFromList->Speed;
	strcpy(ship->Notes, shipFromList->Notes);
	ship->Left = NULL;
	ship->Right = NULL;
	ship->deleteFlag = 0;
}		/* -----  end of function newShip  ----- */

/* HIER UNTEN DIE ENTWUERFE DIE ICH ENDLIOCH NICHT BENUTZT HABE, WEIL SIE FUNKTIONIEREN NICHT */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  fromListToTree
 *  Description:  Funktion die erzeugt Baum aus der Verkettete Liste
 *  Arguments: 	  Zeiger auf erster Element der Verkettete Liste, Anzahl der Elemente
 *  Return Value: Zeiger auf der wurzel der neuen Baum
 * =====================================================================================
 */   /*  
Ship* fromListToTree(LLElement* LLFirstElement, unsigned int counter){
	LLElement* current;
	Ship* ShipToAdd;
	Ship* newroot;
	current = LLFirstElement;
	int i;

	if (counter > 1){
	printf("Anzahl ist: %d\n", counter );
	*  in der Mitte gehen und es zum Wurzel machen *
	int mid = counter/2 + 1;
	printf("mid ist: %d\n", mid);
	
	for(i = 0; i < mid; i++){ //Den Passenden Shiff finden
		if (current->Counter == mid){
			printf("i ist: %d\n", i);
		       	ShipToAdd = current->Ship;
			printf("Den Richtigen Shiff ist gefunden geworden\n");
			}
		else if (current->Next == NULL) break;
	       	else current = current->Next;
		}
	newroot  = ShipToAdd;
	
	printf("ISt erflograich zum Baum eddiert geworden\n");
         *  In der Linke und Rechte Zweige einfuegen *
        while (mid > 1) {
		printf("inder linken Zwieg gehe\n");
		newroot->Left  = fromListToTree(current, mid-1);
        	}
	while (mid < counter){
		printf("inder rechten Zwieg gehe\n");
		newroot->Right  = fromListToTree(current, mid+1);
		}               
	}
        return newroot;
}*/		/* -----  end of function fromListToTree  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  addToList
 *  Description:  Funktion die Rekursiv addiert Elemente aus der Baum zur 
 *  		  eine Verkette Liste
 *  Arguments:    Zeiger auf der Wurzel der Baum, Zeiger auf der Anfang der
 *  		  Verkette Liste
 *  Return value: Zeiger auf der erster Element der Liste
 * =====================================================================================
 *
void addToList(Ship* root, LLElement* LLFirstElement){
	
	Ship* current;
	current = root;

	if ( current->Left != NULL){ 
		addToList(current->Left, LLFirstElement);
		}
		
	addToList(current, LLFirstElement);
		
	if ( current->Right != NULL){ 
		addToList(current->Right, LLFirstElement);
		}
	
}		* -----  end of function addToList  ----- */
