/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Haupt Datei der Program
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "menu.h"
#include "lib.h"
#include "ships.h"
#include "datastructure.h"
#include "balancing.h"
#define SLEEPTIME 3 //darf nich kleine als 1 sein!

int main(){
	int input;
	char *menuTitel = "Verwaltung der Schiffe";
	char *menuItems[] = {
		"Neues Schiff hinzufuegen", // "Neues Shiff hinzufuegen",
		"Vorhandenes Shiff loeschen",
		"Alle Shiffe auflisten",
		"Balanziereni (leider nur FAST fertig)",
		"Shiff suchen",
		"Geschwindigkeit",
		"Luftlinie",
		"Graphisch der Baum darstellen",
		"Linked List ausdrucken",
		"Beenden"};


	Ship* rootship = NULL;
	Ship* newrootship = NULL;
	LLElement* LLFirstElement = NULL; //erster Shiff in der Verkette Liste
	gotoXY(1,21);
 	while ((input = getMenu(menuTitel, menuItems, 10)) != 0){ ;
		switch(input){
			case 1: rootship = addShip(rootship);sleep(SLEEPTIME-1); break; 
			case 2: rootship = deleteShip(rootship);break;
			case 3: printTreeLWR(rootship); sleep(SLEEPTIME);break;
			case 4: rootship = balance(rootship); break;
			case 5: findShip(rootship); sleep(SLEEPTIME); break; ;
			case 6: compareSpeed(rootship); sleep(SLEEPTIME); break;
			case 7: showDistance(rootship); sleep(SLEEPTIME); break;
			case 8: graphicPrintTree(rootship, 4); sleep(SLEEPTIME);break;
			case 9: LLFirstElement = convertTreeToList(LLFirstElement, rootship);
				countElements(LLFirstElement);
				printList(LLFirstElement);
				sleep(SLEEPTIME);break;
			}
	}

	return EXIT_SUCCESS;
}
