/*
 * =====================================================================================
 *
 *       Filename:  balancing.h
 *
 *    Description:  Header Datei fuer die Funktionen der Balanzierung des Baumes 
 * =====================================================================================
 */

#ifndef BALANCING_H 
#define BALANCING_H 
#include "datastructure.h"

LLElement* createList(Ship* root);
LLElement* convertTreeToList (LLElement* LLFirstElement, Ship* root);
LLElement* deleteMarkedFromLL(LLElement* LLFirstElement);
int countElements(LLElement* LLFirstElement);
LLElement* createList(Ship* root); //,LLElement* LLFirstElement);
LLElement* appendShip(LLElement* LLFirstElement, Ship* ship);
void printList(LLElement* LLFirstElement);
LLElement* createElement(Ship* ship);
Ship* fromListToTree(LLElement* LLFirstElement, unsigned int Anzahl);
Ship* balance(Ship* oldroot);
Ship* newShip(Ship* shipFromList);
Ship* sortedListToBST(LLElement* LLFirstElement, int counter);

#endif   /* ----- #ifndef ships_INC  ----- */
