/*
 * =====================================================================================
 *
 *       Filename:  datastructure.h
 *
 *    Description:  Datenstrukturen
 * =====================================================================================
 */

#ifndef  DATASTRUCTRE_H
#define  DATASTRUCTRE_H
#define MAXNAME 26
#define MAXNOTES 40
typedef struct{
	float x;
	float y;
	float z;
}Coordinates;

typedef struct Ship{
	char Name[MAXNAME];
	Coordinates* Position;
	float Speed;
	char Notes[MAXNOTES];
	struct Ship* Left;
	struct Ship* Right;
	unsigned int deleteFlag; //1- wenn geloescht sein soll, 0-wenn es bleiben soll
}Ship;

typedef struct LLElement{
	struct Ship* Ship;
	int Counter;
	struct LLElement* Next;
//	struct Ship* Prev;
}LLElement;

#endif   /* ----- #ifndef datastructure_INC  ----- */
