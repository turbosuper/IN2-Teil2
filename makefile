#  A.Zimmermann, Allzweck-Makefile
#  Vorteile:
#  * Nur geänderte C- und H-Dateien werden kompiliert.
#  * C- und O-Dateien werden getrennt gespeichert.
#
#------------------------------

# Program name.
ZIEL = prog

# Source codes in working directory.
#SRC = start.c methoden.c lib.c
#HDR = adt.h
# If all c-files needed,
# and if c-files are always presented,
# otherwise the SRC is empty.
SRC = $(shell ls *.c)
HDR = $(shell ls *.h)

# Directory for obj-files.
DIR = obj

#------------------------------

# Pattern for obj-files.
OBJ_PAT = $(DIR)/%.o
# List of obj-files.
OBJ_DAT = $(SRC:%.c=$(OBJ_PAT))
# Pattern for obj-files as out in gcc.
OBJ_OUT = $(<:%.c=$(OBJ_PAT))

# Linker
$(ZIEL) : $(OBJ_DAT)
	@echo Linking ...
	gcc -o $@ $(OBJ_DAT) -lm
	
# Compiler
$(OBJ_PAT) : %.c $(HDR)
	@echo Compiling ...
	gcc -c -o $(OBJ_OUT) $<
	
run:
	clear
	./$(ZIEL)

clean:
	/bin/rm -f $(DIR)/*.o

# Link obj-files only
link:
	@echo Linking obj-files only
	gcc -o $(ZIEL) $(shell ls $(DIR)/*.o)

#  =============  EOF  =============

