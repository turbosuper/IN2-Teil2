/*
 * =====================================================================================
 *
 *       Filename:  ships.h
 *
 *    Description:  Header Datei fuer die Funktionen der Shiff Verwaltung
 * =====================================================================================
 */

#ifndef SHIPS_H 
#define SHIPS_H 
#include "datastructure.h"


void dummyFunc();
//Ship* appendShip(Ship*);
Ship* addShip(Ship* root);
void printShip(Ship*);
void printPosition(Coordinates*);
Ship* createShip();
Coordinates* createPosition();
//void showAll(Ship*);
void findShip(Ship*);
Ship* getShip (Ship* shipone, char keyword[MAXNAME]);
Ship* deleteShip(Ship*);
void compareSpeed (Ship* root);
void printName (Ship* ship);
void showDistance (Ship* root);
Coordinates* getPosition(Ship* ship);
float calculateDistance(Coordinates* position1, Coordinates* position2);
Ship* sortShips(Ship* shipone);
Ship* swapShips(Ship* ship1, Ship* ship2);
void graphicPrintTree(Ship* root, int ident);
void printTreeWLR(Ship* root);
Ship* insertShip(Ship* akt, Ship* new);
Ship* listElementsWLR (Ship* current);
void printTreeRWL(Ship* root);
void printTreeLWR(Ship* root);
void listFasterShips(Ship* current, float speed);
#endif   /* ----- #ifndef ships_INC  ----- */
