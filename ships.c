/*
 * =====================================================================================
 *
 *       Filename:  ships.c
 *
 *    Description:  Funktione zur Shiffe Verwaltung
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ships.h"
#include "lib.h"
#include "datastructure.h"
#define SPACES 6

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  dummFunc
 *  Description:  
 * =====================================================================================
 */
void dummyFunc(){
	printf("Noch nicht Fertig.\n");
}		/* -----  end of function dummFunc  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  createCoordinates
 *  Description:  Liest von Tastutur ab, und spiechert die Angaben in eine Postion struct
 *  Arguments:	  keine 
 *  Return value: Zeiger auf der Adresse wo die Koordinen gespiechert sind
 *
 * =====================================================================================
 */
Coordinates* createPosition(){
	Coordinates* coordinates;
	if ((coordinates = (Coordinates*)malloc(sizeof(Coordinates))) == NULL){
		printf("Speicher Zugriff Fehler!\n");
		exit(1);
		}

	printf("Bitte geben sie der Position X des Shiffes ein: \n");
	scanf("%f", &coordinates->x);
	printf("Bitte geben sie der Position Y des Shiffes ein: \n");
	scanf("%f", &coordinates->y);
	printf("Bitte geben sie der Position Z des Shiffes ein: \n");
	scanf("%f", &coordinates->z);
	return coordinates;
}		/* -----  end of function createCoordinates  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  createShip
 *  Description:  Liest von Tastutur ab, und spiechert die Angaben in eine Ship struct
 *  Arguments:	  keine 
 *  Return value: Zeiger auf der Adresse wo die Shiff gemacht ist

 *
 * =====================================================================================
 */
Ship* createShip(){
	Ship* ship;
	ship = (Ship*)malloc(sizeof(Ship));
	if ((ship = (Ship*)malloc(sizeof(Ship))) == NULL){
		printf("Speicher Zugriff Fehler!\n");
		exit(1);
		}

	printf("Bitte geben sie der Name des Shiffes ein: (langste moegliche name ist %d Zeichen gross)\n", MAXNAME);
	fgets(ship->Name, MAXNAME, stdin);
	kbclr();
	printf("Bitte geben sie die Geschwindigketi des Schiffes: \n");
	scanf("%f", &ship->Speed);
	kbclr();
	printf("Bitte geben sie die Notizen bezueglich der Shiff ein: (langste moegliche ist %d)\n", MAXNOTES);
	fgets(ship->Notes, MAXNOTES, stdin);
	ship->Position = createPosition(); 
	ship->Right = NULL;
	ship->Left = NULL;
	ship->deleteFlag = 0;
	return ship;
}		/* -----  end of function createShip  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  printPosition
 *  Description:  Zeigt der LEga und Korrdinaten des Shicffes
 *  Arguments:	  Zeiger auf der Struktur der Kooridanten
 *  Return value: Keine
 * =====================================================================================
 */
void printPosition(Coordinates* position){
	if (position == NULL) printf("postion ist null \n");
	printf("X: %4.3f, Y %4.3f, Z: %4.3f \n", position->x, position->y, position->z) ;
}		/* -----  end of function showShip  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  printShip
 *  Description:  Alle Daten zu einen Shiff zeigen
 *  Arguments:	  Zeiger auf der Struktur der beinhaltet einen Shiff
 *  Return value: Keine
 * =====================================================================================
 */
void printShip (Ship* Ship){
	if (Ship != NULL && Ship->deleteFlag != 1){
 		printf("==================\n");
 		printf("Name: %sGeschwindigkeit %4.3f \nNotizen: %s", Ship->Name, Ship->Speed, Ship->Notes) ;
		printf("Lage des Shiffes:");
		printPosition(Ship->Position); 
	//	printf("\ndeleteFlag: %u\n" , Ship->deleteFlag);
		printf("\n");
		}
	else{
		printf("Keine Shiff zum Ausdrucken\n");
	}	
}		/* -----  end of function showShip  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  insertShip
 *  Description:  Eine Neue Shiff inserten zur einer Platz in der Baum
 *  Arguments:	  Zeiger auf der Wurzel der Baum, und Zeiger auf der Shif der inserted sein soll
 *  Return Value: Zeiger auf der Wurzel
 * =====================================================================================
 */
Ship* insertShip(Ship* akt, Ship* new){

	int j = strcmp(new->Name, akt->Name);
		
	if (j<0){
		if(akt->Left == NULL ) akt->Left = new;
		else insertShip(akt->Left, new);
		}
	if (j>0){
		if(akt->Right == NULL ) akt->Right = new;
		else insertShip(akt->Right, new);
		}
	return akt;

}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  addShip
 *  Description:  Addiert ein Ship zur Baum
 *  Arguments: 	  Wurzel der Baum
 *  Return Value: Wurzel der Baum
 * =====================================================================================
 */
Ship* addShip(Ship* root){

	Ship* new;

	if(root == NULL){
	       	root = createShip();
		}
	else{
		new = createShip();
		insertShip(root, new);
		}

	printf("\nErfolg!\n");
	return root;

}		/* -----  end of function addShip  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  getShip
 *  Description:  Findet einen Shiff, und liefert den Strukt Shiff zurueck
 *  Arguments:	  Zeiger auf Wurzel des Baumes, gesuchte Name des Shiffes
 *  Retunr Value: Zeiger auf dem Strukt mit der gesuchte Shiff
 * =====================================================================================
 */

Ship* getShip(Ship* root, char keyword[MAXNAME]){
 
	if (root == NULL || (strcmp(keyword, root->Name)==0))
		return root;

	if(strcmp(keyword, root->Name) > 0)
		return getShip(root->Right, keyword);

	return getShip(root->Left, keyword);
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  findShip
 *  Description:  Druck aus der gesuchte Shiff
 *  Arguments: 	  Zeiger auf Wurzel der Baum
 *  Return value: Keine
 * =====================================================================================
 */
void findShip(Ship* root)
{
	
	char keyword[MAXNAME];

	if (root == NULL){
		printf("Baum ist leer \n");
		return;
		}

	printf("Bitte geben sie den gesuchte name in: \n");
	fgets(keyword, MAXNAME, stdin); 

	/*  wenn es keinen gibt ODER wenn es zum loeschen markiert ist geworden  */
	if (getShip(root, keyword)== NULL || (getShip(root, keyword))->deleteFlag == 1){ 
		printf("Shiff mit der Name:  \n %sist nicht gefunden geworden \n", keyword);
		}
	else{
		printf("Der gesuchte Shif: \n");
		printShip(getShip(root, keyword));
		}
}	
/* -----  end of function findShip  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  printName
 *  Description:  Funkion der gibt Name der Gegebene struct
 *  Arguments:    Zeiger auf dem Strukt mit Shiff
 *  Return value: Keine
 * =====================================================================================
 */
void printName (Ship* ship){
	if (ship == NULL){
		printf("Fehler, keine Shiff gegeben\n");
	}else{
		printf("%s\n", ship->Name);
	}
}		/* -----  end of function printName  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  deleteShip
 *  Description:  Findet einen Shiff, dann setzt seiner deleteFlag auf 1 (wenn es gefunden ist)
 *  Arguments: 	  Zeiger auf anfang der Verkette Liste
 *  Return Value: Zeiger auf Anfang der Verkette Liste
 * =====================================================================================
 *
 *
 */
Ship* deleteShip(Ship* root){
	char keyword[MAXNAME];
	if (root == NULL){
		printf("Der Baum ist leer \n");
		return root;
		}
	
	printf("Bitte geben sie Name der Shiff, der geloescht sein soll: \n");
	fgets(keyword, MAXNAME, stdin); 

	if (getShip(root, keyword)== NULL){
		printf("Shiff mit der Name:  \n%sist nicht gefunden geworden, da kann er auch nicht geloescht sein! \n", keyword);
		sleep(3);
		return root;
		}
	else{  
		(getShip(root, keyword))->deleteFlag = 1;
		printf("Erfolg! Shiff ist zum loeschen markiert\n");
		sleep(3);
		return root;
		}
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  compareSpeed
 *  Description:  Fragt nach der eingabe, und listet Namen aller Shiffe die shneller/gleich sind
 *  Arguments:    Zeiger auf der Wurzel der 
 *  Retrun Wert:  keine
 * =====================================================================================
 */
void compareSpeed(Ship* root){
	float speed;

	if( root == NULL ){
		printf("Liste ist leer\n");
	}else{
		printf("Bitte geben sie eine Geschwindigkeit, mit denem Shiffe vergleichen sein sollten: \n");
		scanf("%f", &speed);
		printf("Diese Shiffe sind schneller als %.4f : \n", speed);
		listFasterShips(root, speed);
		}
	
}		/* -----  end of function compareSpeed  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  listFasterShips
 *  Description:  Listet Shiffe schneller, als die gegebene geschwindikeit
 *  Arguments:    Zieger auf Wurzel der Baum, Geschwindikeit als Float
 *  Return Value: Keine
 * =====================================================================================
 */
void listFasterShips(Ship* current, float speed){

		if(current->Speed > speed && current->deleteFlag != 1 ) printName(current);

		if ( current->Left != NULL){ 
			listFasterShips(current->Left, speed);
			}
		if ( current->Right != NULL){ 
			listFasterShips(current->Right, speed);
			}
		
}		/* -----  end of function listFasterShips  ----- */
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  getPosition
 *  Description:  Liefert Zurueck Strukt mit Koordinaeten des Shciffes
 *  Arguments:    Zeiger auf der Struct der Shiff
 *  Return Value  Zeiger auf die Koordinaten
 * =====================================================================================
 */
Coordinates* getPosition(Ship* ship){
	return ship->Position;
}		/* -----  end of function getPosition  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  calculateDistance
 *  Description:  Rechnet der Distanz zwischen zwei Punkten
 *  Arguments: 	  Zeigern auf zwei Kooridanten strukturen
 *  Return Value: Distanz als float
 * =====================================================================================
 */
float calculateDistance(Coordinates* position1, Coordinates* position2){
	float X1, Y1, Z1, X2, Y2, Z2, result;
	X1 = position1->x; 
	Y1 = position1->y; 
	Z1 = position1->z; 
	X2 = position2->x;
	Y2 = position2->y;
	Z2 = position2->z;

	result = sqrt((float)(pow((X1-X2),2))+(float)(pow((Y1-Y2),2))+(float)(pow((Z1-Z2),2)));
	return result;
}		/* -----  end of function showDistance  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  showDistance
 *  Description:  Erfragt Nutzer nach Namen der Schiffe, dann sucht die Schiffe aus und
 *  		  letztendlich zeigt der Distanz zwischen zwei Schiffe
 *  Arguments:    Anfang der Verkette Liste
 *  Return Value: keine
 * =====================================================================================
 */
void showDistance(Ship* rootship){
	Coordinates* firstPosition = NULL;
	Coordinates* secondPosition = NULL;
	char keyword[MAXNAME];
	char keyword2[MAXNAME];

	if (rootship == NULL){
		printf("Liste ist leer\n");
	}else{
		while(firstPosition == NULL){
			printf("Bitte geben sie den gesuchte Shiff 1: \n");
			fgets(keyword, MAXNAME, stdin); 
			/*Nur dann die Position nehmen, wenn Shiff existiret, und nicht markiert ist */
			if (getShip(rootship, keyword)== NULL || getShip(rootship, keyword)->deleteFlag == 1){
				printf("Shiff mit der Name:  \n %sist nicht gefunden geworden! \n", keyword);
				printf("Versuchen sie es bitte erneut.\n\n");
			}else{
				firstPosition = getPosition(getShip(rootship, keyword));
				}
			}
		
		while(secondPosition == NULL){
			printf("Bitte geben sie den gesuchte Shiff 2: \n");
			fgets(keyword2, MAXNAME, stdin); 

			if (getShip(rootship, keyword2)== NULL || getShip(rootship, keyword2)->deleteFlag == 1){
				printf("Shiff mit der Name:  \n %sist nicht gefunden geworden! \n", keyword2);
				printf("Versuchen sie es bitte erneut.\n\n");
			}else{
				secondPosition = getPosition(getShip(rootship, keyword2));
				}
			}
		printf("=======================================================\n");
		printf("\nDer Distanz zwischen zwie gegebene Shiffe ist: %.3f \n", calculateDistance(firstPosition, secondPosition));

		}
}		/* -----  end of function showDistance  ----- */




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  graphicPrintTree
 *  Description:  Zeigt der ganze Baum
 *  Arguments: 	  Zeiger auf der Wurzel der Baum, Anzahl der leerzeichen fuer Grafische 
 *  		  Darstellung.
 *  Return value: Keine
 * =====================================================================================
 */
void graphicPrintTree(Ship* root, int ident){
	
	Ship* current;
	current = root;
	int i;
	
	if (root == NULL) return;

	ident += SPACES;  //der Distanz zwischen Ebene erhoehen

	graphicPrintTree(current->Right, ident); //Erst die Rechte Knote ausdrucken
	
	printf("\n");
	
	for(i = SPACES; i < ident; i++){
	       	printf(" ");
       		}
	printName(current);

	graphicPrintTree(current->Left, ident);

}	/* -----  end of function graphicPrinTree  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  printTreeRWL
 *  Description:  Listet Alle Elemente der Baum nach dieser Wiese: rechten Element, Wurzel
 *  		  linken Element.	
 *  Arguments: 	  Zeiger auf der Wurzel der Baum
 *  Return value: Keine
 * =====================================================================================
 */
void printTreeRWL(Ship* root){ 

	Ship* current;
	current = root;
	
	if (root == NULL) printf("Baum ist leer, nichts zu Ausdrucken\n");
	else{

		if ( current->Right != NULL){ 
			printTreeRWL(current->Right);
			}

		printShip(current);
		
		if ( current->Left != NULL){ 
			printTreeRWL(current->Left);
			}
		}
}		 /*  -----  end of function printTree  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  printTreeWRL
 *  Description:  Listet Alle Elemente der Baum nach dieser Wiese: Wurzel, rechten Element,
 *  		  linken Element.	
 *  Arguments: 	  Zeiger auf der Wurzel der Baum
 *  Return value: Keine
 * =====================================================================================
 */
void printTreeWLR(Ship* root){  //DIESE FUNKTION FUNKTIONIERT ABER ICH WOLLTE ETWAS ANDERESE VERSUCHN
	
	Ship* current;
	current = root;
	
	if (root == NULL) printf("Baum ist leer, nichts zu Ausdrucken\n");
	else{
		printShip(current);

		if ( current->Left != NULL){ 
			printTreeWLR(current->Left);
			}
		if ( current->Right != NULL){ 
			printTreeWLR(current->Right);
			}
		}
}		 /*  -----  end of function printTree  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  printTreeLWR
 *  Description:  Listet Alle Elemente der Baum nach dieser Wiese: Links, Wurzel, 
 *  		  rechten Element,
 *  Arguments: 	  Zeiger auf der Wurzel der Baum
 *  Return value: Keine
 * =====================================================================================
 */
void printTreeLWR(Ship* root){  //DIESE FUNKTION FUNKTIONIERT ABER ICH WOLLTE ETWAS ANDERESE VERSUCHN
	
	Ship* current;
	current = root;
	
	if (root == NULL) printf("Baum ist leer, nichts zu Ausdrucken\n");
	else{

		if ( current->Left != NULL){ 
			printTreeLWR(current->Left);
			}
		
		printShip(current);
		
		if ( current->Right != NULL){ 
			printTreeLWR(current->Right);
			}
		}
}		/* -----  end of function printTreeLWR  ----- */
